## Overview

This project was implemented from [Udemy React Course](https://www.udemy.com/share/101XgIB0obcFdWR3Q=/)
It is an app where the user can create an expense, view his expenses and filter them by date.

## Demo

[click here](https://expensify-app-e5464.web.app/) to view a deployed version of the app.

## Dependencies

-   node
-   yarn

## firebase Database:

-   A google account is required for this operation.
-   create two realtime firebase databases.One for testing and one for prodiction and copy rules configurations in the file firebase-DB-rules.json.
-   Create a web app in firebase console and connect it to the database.
-   Enable sign in as google in firebase authentications section.
-   create .env.development and .env.test files in the root directory and fill them in with the following:

*   FIREBASE_API_KEY=
*   FIREBASE_AUTH_DOMAIN=
*   FIREBASE_DATABASE_URL=
*   FIREBASE_PROJECT_ID=
*   FIREBASE_STORAGE_BUCKET=
*   FIREBASE_MESSAGING_SENDER_ID=
*   FIREBASE_APP_ID=
*   FIREBASE_MEASUREMENT_ID=

## Install

`npm install`

### Start

`npm run dev`

### Test

`yarn run test`

### Build

`npm run build:prod`

### Start Production build

`npm run start`

Open [http://localhost:8080](http://localhost:8080) to view it in the browser.
