import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {
    startAddExpense,
    addExpense,
    editExpense,
    removeExpense,
    setExpenses,
    startSetExpenses,
    startRemoveExpense,
    startEditExpense,
} from '../../actions/expenses';
import expenses from '../fixtures/expenses';
import database from '../../firebase/firebase';
import uuid from 'uuid';

const createMockStore = configureMockStore([thunk]);
const uid = 'fakeuid';
const defaultAuthState = { auth: { uid } };

beforeEach(done => {
    const expensesData = {};
    expenses.forEach(({ id, description, note, amount, createdAt }) => {
        expensesData[id] = { description, note, amount, createdAt };
    });
    database
        .ref(`users/${uid}/expenses`)
        .set(expensesData)
        .then(() => done());
});

test('should setup remove expense action', () => {
    const id = uuid();
    const action = removeExpense({ id });
    expect(action).toEqual({
        type: 'REMOVE_EXPENSE',
        id,
    });
});

test('should setup not remove expense action', () => {
    const action = removeExpense();
    expect(action).toEqual({
        type: 'REMOVE_EXPENSE',
        id: undefined,
    });
});

test('should remove expense from db', done => {
    const store = createMockStore(defaultAuthState);
    const { id } = expenses[0];
    store
        .dispatch(startRemoveExpense({ id }))
        .then(() => {
            const actions = store.getActions();
            expect(actions[0]).toEqual({
                type: 'REMOVE_EXPENSE',
                id,
            });
            return database.ref(`users/${uid}/expenses/${id}`).once('value');
        })
        .then(snapshot => {
            expect(snapshot.val()).toBeFalsy();
            done();
        });
});

test('should setup edit expense action', () => {
    const { id } = expenses[2];
    const action = editExpense(id, { note: 'new note' });

    expect(action).toEqual({
        type: 'EDIT_EXPENSE',
        id,
        updates: { note: 'new note' },
    });
});

test('should edit expense from db', done => {
    const store = createMockStore(defaultAuthState);
    const { id } = expenses[0];
    const updates = { amount: 299 };
    store
        .dispatch(startEditExpense(id, updates))
        .then(() => {
            const actions = store.getActions();
            expect(actions[0]).toEqual({
                type: 'EDIT_EXPENSE',
                id,
                updates,
            });
            return database.ref(`users/${uid}/expenses/${id}`).once('value');
        })
        .then(snapshot => {
            expect(snapshot.val().amount).toBe(updates.amount);
            done();
        });
});

test('should setup add expense action', () => {
    const action = addExpense(expenses[2]);

    expect(action).toEqual({
        type: 'ADD_EXPENSE',
        expense: expenses[2],
    });
});

test('should add expense to database and store', done => {
    const store = createMockStore(defaultAuthState);
    const expense = {
        description: 'Mouse',
        amount: 3000,
        note: 'this one is good',
        createdAt: 1000,
    };
    store
        .dispatch(startAddExpense(expense))
        .then(() => {
            const actions = store.getActions();
            expect(actions[0]).toEqual({
                type: 'ADD_EXPENSE',
                expense: {
                    id: expect.any(String),
                    ...expense,
                },
            }); //return a promise in order to be able to be read from the next promise
            return database
                .ref(`users/${uid}/expenses/${actions[0].expense.id}`)
                .once('value');
        })
        .then(snapshot => {
            expect(snapshot.val()).toEqual(expense);
            done(); // a way to let jest know that this test is async
        });
});

test('should add expense with defaults to database and store', done => {
    const store = createMockStore(defaultAuthState);
    const expenseDefault = {
        description: '',
        amount: 0,
        note: '',
        createdAt: 0,
    };
    store
        .dispatch(startAddExpense({}))
        .then(() => {
            const actions = store.getActions();
            expect(actions[0]).toEqual({
                type: 'ADD_EXPENSE',
                expense: {
                    id: expect.any(String),
                    ...expenseDefault,
                },
            }); //return a promise in order to be able to be read from the next promise
            return database
                .ref(`users/${uid}/expenses/${actions[0].expense.id}`)
                .once('value');
        })
        .then(snapshot => {
            expect(snapshot.val()).toEqual(expenseDefault);
            done(); // a way to let jest know that this test is async
        });
});

test('should setup set expense data with object', () => {
    const action = setExpenses(expenses);
    expect(action).toEqual({
        type: 'SET_EXPENSES',
        expenses,
    });
    test('should setup set expense data with object', () => {
        const action = setExpenses(expenses);
        expect(action).toEqual({
            type: 'SET_EXPENSES',
            expenses,
        });
    });
});

test('should fetch expenses from db', done => {
    const store = createMockStore(defaultAuthState);
    store.dispatch(startSetExpenses()).then(snapshot => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: 'SET_EXPENSES',
            expenses,
        });
        done();
    });
});
