import React from 'react';
import selectExpensesTotal from '../../selectors/expenses-total';
import expenses from '../fixtures/expenses';

test('should return a sum zero sum when passing empty array', () => {
    const total = selectExpensesTotal([]);
    expect(total).toBe(0);
});

test('should return the amount of one expense', () => {
    const total = selectExpensesTotal([expenses[0]]);
    expect(total).toBe(expenses[0].amount);
});

test('should return expense sum', () => {
    const total = selectExpensesTotal(expenses);
    expect(total).toBe(114195);
});
